<?php

namespace Qionar\Coingecko;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

use Qionar\Coingecko\Endpoints\AssetPlatform;
use Qionar\Coingecko\Endpoints\Categories;
use Qionar\Coingecko\Endpoints\Coins;
use Qionar\Coingecko\Endpoints\Companies;
use Qionar\Coingecko\Endpoints\Contract;
use Qionar\Coingecko\Endpoints\Derivatives;
use Qionar\Coingecko\Endpoints\ExchangeRates;
use Qionar\Coingecko\Endpoints\Exchanges;
use Qionar\Coingecko\Endpoints\GlobalData;
use Qionar\Coingecko\Endpoints\Indexes;
use Qionar\Coingecko\Endpoints\Ping;
use Qionar\Coingecko\Endpoints\Search;
use Qionar\Coingecko\Endpoints\Simple;

use Exception;
use Qionar\Coingecko\Endpoints\Trending;
use ReflectionException;

// TODO: analyze dependencies only for laravel or use with native php... create
// TODO: last maked endpoint exhangesRates
// TODO:

class Coingecko
{

    public function ping(): Ping
    {
        return new Ping();
    }

    public function simple(): Simple
    {
        return new Simple();
    }

    public function coins(): Coins
    {
        return new Coins();
    }

    public function contract(): Contract
    {
        return new Contract();
    }

    public function assetPlatform(): AssetPlatform
    {
        return new AssetPlatform();
    }

    public function categories(): Categories
    {
        return new Categories();
    }

    public function exchanges(): Exchanges
    {
        return new Exchanges();
    }

    public function indexes(): Indexes
    {
        return new Indexes();
    }

    public function derivatives(): Derivatives
    {
        return new Derivatives();
    }

    public function exchangeRates(): ExchangeRates
    {
        return new ExchangeRates();
    }

    public function search(): Search
    {
        return new Search();
    }

    public function trending(): Trending
    {
        return new Trending();
    }

    public function globalData(): GlobalData
    {
        return new GlobalData();
    }

    public function companies():Companies
    {
        return new Companies();
    }

}

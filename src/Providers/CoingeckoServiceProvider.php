<?php

namespace Qionar\Coingecko\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Qionar\Coingecko\Coingecko;

class CoingeckoServiceProvider extends ServiceProvider
{

    public function register()
    {
        App::bind('coingecko', function () {
           return new Coingecko();
        });
    }

    public function boot(): void
    {
        //
    }
}

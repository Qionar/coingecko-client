<?php

namespace Qionar\Coingecko\Endpoints;

class Exchanges extends BaseEndpoint
{

    public function exchanges(int $perPage = 100, int $page = 1)
    {
        $url = self::getUrl('/exchanges', "?per_page=${$perPage}&page=${$page}");

        $this->execute($url);
    }

    public function exchangesList()
    {
        $url = self::getUrl('/exchanges/list');

        $this->execute($url);
    }

    public function exchangesVolumeById(int $id)
    {
        $url = self::getUrl("/exchanges/${$id}");

        $this->execute($url);
    }

    public function exchangesVolumeTickers(
        int $id,
        array $coinIds = [],
        string $includeExchangeLogo = 'false',
        int $page = 1,
        string $depth = 'false',
        string $order = ''
    )
    {

        $coinIds = implode(',', $coinIds);

        $url = self::getUrl(
               "/exchanges/${$id}/tickers",
             "?coin_ids=${$coinIds}" .
                        "&include_exchange_logo=${$includeExchangeLogo}" .
                        "&page=${$page}" .
                        "&depth=${$depth}" .
                        "order=${$order}"
        );

        $this->execute($url);
    }

    public function exchangesVolumeChart(int $id, int $days)
    {
        $url = self::getUrl("/exchanges/${$id}", "?days=${$days}");

        $this->execute($url);
    }

}

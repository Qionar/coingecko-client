<?php

namespace Qionar\Coingecko\Endpoints;

class Trending extends BaseEndpoint
{

    public function trending(): Trending
    {
        $url = self::getUrl('/search/trending');

        return $this->execute($url);
    }

}

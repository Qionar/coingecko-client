<?php

namespace Qionar\Coingecko\Endpoints;

class Search extends BaseEndpoint
{

    public function search($queryString)
    {
        $url = self::getUrl('/search', "?query=${$queryString}");

        return $this->execute($url);
    }

}

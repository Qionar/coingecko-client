<?php

namespace Qionar\Coingecko\Endpoints;

class Indexes extends BaseEndpoint
{
    public function indexes(int $perPage = 100, int $page = 1)
    {
        $url = self::getUrl('/indexes', "?per_page=${$perPage}&page=${$page}");

        $this->execute($url);
    }

    public function indexesMarket(int $marketId, int $id)
    {
        $url = self::getUrl("/indexes/${$marketId}/${$id}");

        $this->execute($url);
    }

    public function indexesList()
    {
        $url = self::getUrl('/indexes/list');

        $this->execute($url);
    }

}

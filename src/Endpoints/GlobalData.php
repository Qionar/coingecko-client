<?php

namespace Qionar\Coingecko\Endpoints;

class GlobalData extends BaseEndpoint
{

    public function globalData(): GlobalData
    {
        $url = self::getUrl('/global');

        return $this->execute($url);
    }

    public function globalDeFiData(): GlobalData
    {
        $url = self::getUrl('/global/decentralized_finance_defi');

        return $this->execute($url);
    }

}

<?php

namespace Qionar\Coingecko\Endpoints;

class Categories extends BaseEndpoint
{

    public function categoriesList()
    {
        $url = self::getUrl('/coins/categories/list');

        $this->execute($url);
    }

    public function categories(string $order)
    {
        $url = self::getUrl('/coins/categories', "?order=${$order}");

        $this->execute($url);
    }

}

<?php

namespace Qionar\Coingecko\Endpoints;

use Illuminate\Support\Facades\Http;
use Qionar\Coingecko\Interfaces\EndpointInterface;
use \Illuminate\Http\Client\Response;

abstract class BaseEndpoint implements EndpointInterface
{

    private static $url = 'https://api.coingecko.com/api/';
    private static $version = 'v3';
    // default - return response after executing method
    // stored - access to storedMethods, return $this after request and access to run other methods
    // raw - return raw http class
    private string $typeOfResponse = 'default';

    protected array|null $lastResponse = null;
    protected array|null $responsesArray = null;

    public function stored(): static
    {
        $this->typeOfResponse = 'stored';

        return $this;
    }

    public function raw(): static
    {
        $this->typeOfResponse = 'raw';

        return $this;
    }

    public function getLastResponse(): array|null
    {
        return $this->lastResponse;
    }

    public function getResponses(): array|null
    {
        return $this->responsesArray;
    }

    protected static function getUrl($endPoint = null, $queryString = null): string
    {
        return self::$url . self::$version . $endPoint . $queryString ;
    }

    protected function execute($url): static|array|Response
    {

        if ($this->typeOfResponse === 'stored') {
            $this->makeRequestAndSaveResponse($url);

            return $this;
        }

        if ($this->typeOfResponse === 'raw') {
            return Http::get($url);
        }

        return Http::get($url)->json();
    }

    private function makeRequestAndSaveResponse($url): void
    {
        $result = Http::get($url)->json();
        $this->lastResponse = $result;
        $this->responsesArray[] = $result;
    }

}

<?php

namespace Qionar\Coingecko\Endpoints;

use Exception;

class Coins extends BaseEndpoint
{

    public function list(string $includePlatform = 'false'): Coins
    {
        $url = self::getUrl('/coins/list', "?includePlatform={$includePlatform}");

        return $this->execute($url);
    }

    /**
     * @throws Exception
     */
    public function markets(
        string $vsCurrency,
        array $currencies = [],
        string $category = '',
        string $order = 'market_cap_desc',
        int $perPage = 100,
        int $page = 1,
        string $sparkline = 'false',
        array $priceChangePercentage = []
    ): Coins
    {
        $currencies = implode(',', $currencies);

        $queryString = "?vsCurrency={$vsCurrency}";

        if(strlen($currencies) !== 0) $queryString .= "&ids=${$currencies}";
        if(strlen($category) !== 0) $queryString .= "&category=${$category}";

        $queryString .= "&order=${$order}";
        $queryString .= "&per_page=${$perPage}";
        $queryString .= "&page=${$page}";
        $queryString .= "&sparkline=${$sparkline}";

        if(!empty($priceChangePercentage)){
            $priceChangePercentage = implode(',', $priceChangePercentage);
            $queryString .= "&price_change_percentage=${$priceChangePercentage}";
        }

        $url = self::getUrl('/coins/markets', $queryString);


        return $this->execute($url);
    }

    public function coin(
        int $id,
        string $localization = 'true',
        string $tickers = 'true',
        string $marketData = 'true',
        string $communityData = 'true',
        string $developerData = 'true',
        string $sparkline = 'false',
    ): Coins
    {
        //

        return $this;
    }

    public function coinTickers(
        int $id,
        array $exchangeIds = [],
        string $includeExchangeLogo = '',
        int $page = 1,
        string $order = '',
        string $depth = 'true'
    ): Coins
    {
        //

        return $this;
    }

    public function coinHistory(
        int $id,
        string $date,
        string $localization = 'true',
    ): Coins
    {
        //

        return $this;
    }

    public function coinMarketChart(
        int $id,
        string $vsCurrency,
        string $days,
        string $interval = 'daily'
    ): Coins
    {
        //

        return $this;
    }

    public function coinMarketChartRange(
        int $id,
        string $vsCurrency,
        string $from,
        string $to
    ): Coins
    {
        //

        return $this;
    }

    public function coinOhls(
        int $id,
        string $vsCurrency,
        string $days,
    ): Coins
    {
        //

        return $this;
    }

}

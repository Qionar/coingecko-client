<?php

namespace Qionar\Coingecko\Endpoints;

class Simple extends BaseEndpoint
{

    public function price(
        array $currencies,
        array $vsCurrencies,
        string $marketCap = 'false',
        string $vol24hr = 'false',
        string $change24hr = 'false',
        string $lastUpdate = 'false'
    ): Simple
    {

        $currencies = implode(',', $currencies);
        $vsCurrencies = implode(',', $vsCurrencies);

        $url = self::getUrl(
           "/simple/price",
         "?ids={$currencies}" .
                    "&vs_currencies={$vsCurrencies}" .
                    "&include_market_cap={$marketCap}" .
                    "&include_24hr_vol={$vol24hr}" .
                    "&include_24hr_change={$change24hr}" .
                    "&include_last_updated_at={$lastUpdate}"
        );

        return $this->execute($url);
    }

    public function tokenPrice(
        string $id,
        array $contractAddresses,
        array $vsCurrencies,
        string $marketCap = 'false',
        string $vol24hr = 'false',
        string $change24hr = 'false',
        string $lastUpdate = 'false'
    ): Simple
    {
        $contractAddresses = implode(',', $contractAddresses);
        $vsCurrencies = implode(',', $vsCurrencies);

        $url = self::getUrl(
               "/simple/token_price/{$id}",
             "?contract_addresses={$contractAddresses}" .
                        "&vs_currencies={$vsCurrencies}" .
                        "&include_market_cap={$marketCap}" .
                        "&include_24hr_vol={$vol24hr}" .
                        "&include_24hr_change={$change24hr}" .
                        "&include_last_updated_at={$lastUpdate}"
        );

        return $this->execute($url);
    }

    public function supportedVsCurrencies(): Simple
    {
        $url = self::getUrl('/simple/supported_vs_currencies');

        return $this->execute($url);
    }

}

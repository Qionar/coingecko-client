<?php

namespace Qionar\Coingecko\Endpoints;

class Companies extends BaseEndpoint
{

    public function companiesPublicTreasuryByCoin($id): Companies
    {
        $url = self::getUrl("/companies/public_treasury/${$id}");

        return $this->execute($url);
    }

}

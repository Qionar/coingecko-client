<?php

namespace Qionar\Coingecko\Endpoints;

use Qionar\Coingecko\Facades\Coingecko;

class ExchangeRates extends BaseEndpoint
{

    public function exchangeRates(): ExchangeRates
    {
        $url = self::getUrl('/exchange_rates');

        return $this->execute($url);
    }

}

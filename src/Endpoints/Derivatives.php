<?php

namespace Qionar\Coingecko\Endpoints;

class Derivatives extends BaseEndpoint
{

    public function derivatives(string $includeTickers = 'unexpired')
    {
        $url = self::getUrl('/derivatives', "?include_tickers=${$includeTickers}");

        $this->execute($url);
    }

    public function derivativesExchanges(string $order = '', int $perPage = 100, int $page = 1)
    {
        $url = self::getUrl('/derivatives', "?order=${$order}&per_page=${$perPage}&page=${$page}");

        $this->execute($url);
    }

    public function derivativesExchangesById(int $id, string $includeTickers = 'unexpired')
    {
        $url = self::getUrl("/derivatives/exchanges/${$id}", "?include_tickers=${$includeTickers}");

        $this->execute($url);
    }

    public function derivativesExchangesList()
    {
        $url = self::getUrl('/derivatives/list');

        $this->execute($url);
    }

}

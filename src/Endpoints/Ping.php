<?php

namespace Qionar\Coingecko\Endpoints;

use Illuminate\Support\Facades\Http;

class Ping extends BaseEndpoint
{

    public function ping()
    {
        $url = self::getUrl('/ping');

        return $this->execute($url);
    }

}

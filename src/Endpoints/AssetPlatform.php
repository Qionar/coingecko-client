<?php

namespace Qionar\Coingecko\Endpoints;

class AssetPlatform extends BaseEndpoint
{

    public function assetPlatform()
    {
        $url = self::getUrl('/asset_platform');

        $this->execute($url);
    }

}

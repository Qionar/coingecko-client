<?php

namespace Qionar\Coingecko\Endpoints;

use Illuminate\Support\Facades\Http;
use Qionar\Coingecko\Coingecko;
use Qionar\Coingecko\Interfaces\EndpointInterface;

class Contract extends BaseEndpoint
{

    public function coinContract($id, $contractAddress): Contract
    {
        //

        return $this;
    }

    public function coinContractMarketChart($id, $contractAddress, $vsCurrency, $days): Contract
    {
        //

        return $this;
    }

    public function coinContractMarketChartRange($id, $contractAddress, $vsCurrency, $from, $to): Contract
    {
        //

        return $this;
    }

}

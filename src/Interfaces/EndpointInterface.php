<?php

namespace Qionar\Coingecko\Interfaces;

interface EndpointInterface
{

    public function getLastResponse();

    public function getResponses();

    public function raw();

    public function stored();

}

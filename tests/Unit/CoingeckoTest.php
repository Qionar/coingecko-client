<?php

namespace Qionar\Coingecko\Tests;

use Qionar\Coingecko\Endpoints\AssetPlatform;
use Qionar\Coingecko\Endpoints\Categories;
use Qionar\Coingecko\Endpoints\Coins;
use Qionar\Coingecko\Endpoints\Companies;
use Qionar\Coingecko\Endpoints\Contract;
use Qionar\Coingecko\Endpoints\Derivatives;
use Qionar\Coingecko\Endpoints\ExchangeRates;
use Qionar\Coingecko\Endpoints\Exchanges;
use Qionar\Coingecko\Endpoints\GlobalData;
use Qionar\Coingecko\Endpoints\Indexes;
use Qionar\Coingecko\Endpoints\Ping;
use Qionar\Coingecko\Endpoints\Search;
use Qionar\Coingecko\Endpoints\Simple;
use Qionar\Coingecko\Endpoints\Trending;

use Qionar\Coingecko\Facades\Coingecko;
use Tests\TestCase;

class CoingeckoTest extends TestCase
{

    public function test_that_ping_method_return_ping_instance()
    {
        $ping = Coingecko::ping();

        $this->assertInstanceOf(Ping::class, $ping);
    }

    public function test_that_simple_method_return_simple_instance()
    {
        $simple = Coingecko::simple();

        $this->assertInstanceOf(Simple::class, $simple);
    }

    public function test_that_coins_method_return_coins_instance()
    {
        $coins = Coingecko::coins();

        $this->assertInstanceOf(Coins::class, $coins);
    }

    public function test_that_contract_method_return_contract_instance()
    {
        $contract = Coingecko::contract();

        $this->assertInstanceOf(Contract::class, $contract);
    }

    public function test_that_assetPlatform_method_return_assetPlatform_instance()
    {
        $assetPlatform = Coingecko::assetPlatform();

        $this->assertInstanceOf(AssetPlatform::class, $assetPlatform);
    }

    public function test_that_categories_method_return_categories_instance()
    {
        $categories = Coingecko::categories();

        $this->assertInstanceOf(Categories::class, $categories);
    }

    public function test_that_exchanges_method_return_exchanges_instance()
    {
        $exchanges = Coingecko::exchanges();

        $this->assertInstanceOf(Exchanges::class, $exchanges);
    }

    public function test_that_indexes_method_return_indexes_instance()
    {
        $indexes = Coingecko::indexes();

        $this->assertInstanceOf(Indexes::class, $indexes);
    }

    public function test_that_derivatives_method_return_derivatives_instance()
    {
        $derivatives = Coingecko::derivatives();

        $this->assertInstanceOf(Derivatives::class, $derivatives);
    }

    public function test_that_exchangeRates_method_return_exchangeRates_instance()
    {
        $exchangeRates = Coingecko::exchangeRates();

        $this->assertInstanceOf(ExchangeRates::class, $exchangeRates);
    }

    public function test_that_search_method_return_search_instance()
    {
        $search = Coingecko::search();

        $this->assertInstanceOf(Search::class, $search);
    }

    public function test_that_trending_method_return_trending_instance()
    {
        $trending = Coingecko::trending();

        $this->assertInstanceOf(Trending::class, $trending);
    }

    public function test_that_globalData_method_return_globalData_instance()
    {
        $globalData = Coingecko::globalData();

        $this->assertInstanceOf(GlobalData::class, $globalData);
    }

    public function test_that_companies_method_return_companies_instance()
    {
        $companies = Coingecko::companies();

        $this->assertInstanceOf(Companies::class, $companies);
    }

}

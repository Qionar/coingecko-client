<?php

namespace Qionar\Coingecko\Tests;

use Exception;
use Illuminate\Http\Client\Response;
use Tests\TestCase;
use Qionar\Coingecko\Endpoints\Ping;

class PingTest extends TestCase
{

    public function test_raw_ping_request_return_http_response_client_class()
    {
        $result = (new Ping())->raw()->ping();

        $this->assertInstanceOf(Response::class, $result);
    }

    public function test_default_ping_request_return_array()
    {
        $result = (new Ping())->ping();

        $this->assertTrue(is_array($result));
    }

    public function test_stored_ping_request_store_responses_and_return_last_response()
    {
        $result = (new Ping())->stored()->ping();

        $this->assertNotNull($result->getLastResponse());
    }

    public function test_stored_ping_request_store_responses_and_return_all_responses()
    {
        $result = (new Ping())->stored()->ping()->ping();

        $this->assertNotNull($result->getLastResponse());
        $this->assertNotNull($result->getResponses());
        $this->assertCount(2, $result->getResponses());
    }

}
